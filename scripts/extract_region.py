import pandas as pd
import os
import argparse
from ast import literal_eval


class Dataframe:

    def __init__(self, file_input):
        self.file_input = file_input
    

    def split_rows(self):
        file = pd.read_csv(self.file_input, sep="\t", skiprows=1, header = 0)
        file.explode(['Start', 'End', 'Strand'])

    def extract_infos(self, filename): 
        """
        Select only usefull information based on what we need for intersectbed command line : name, start, end and chr
        """
        name = os.path.basename(filename)
        df_name = name.rsplit('.', 1)[0]

        if filename.endswith('.tsv'): 
            position = ['Start', 'End', 'Chr']
            chromosomes = list(range(1, 13))
            input_file = pd.read_csv(filename, sep="\t", skiprows=1, header = 0)
            split = lambda x: x['Geneid'].rsplit('_', 1)[1]
            input_file["Geneid"] = input_file.apply(split, axis=1)
            for column_name in position : 
                input_file[column_name] = input_file[column_name].str.replace(';', ',')
                input_file[column_name] = input_file[column_name].apply(literal_eval)
            
            input_file = input_file.apply(lambda x: x.explode() if x.name in ["Start", "End", "Chr"] else x).reset_index(drop=True)
            input_file = input_file[input_file.columns[input_file.columns.isin(["Chr", "Start", "End", "Geneid"])]]
            input_file = input_file.reindex(columns=["Chr", "Start", "End", "Geneid"]) ### ok j'ai fini de faire les changements avec explode mais à vérifier si ça fait bien le taff

            
            for number in chromosomes :
                input_file.loc[input_file['Chr'] == number, 'Chr'] = f"chromosome_{number}"

        elif filename.endswith('.bed'):
            chromosomes = list(range(33, 45))
            input_file = pd.read_csv(filename, compression='gzip', sep='\t', header=None)
            input_file = input_file[[0, 1, 2, 3]].drop(index=input_file.index[-2:])
            input_file.columns = ['Chr', 'Start', 'End', 'Score']
            input_file["peak"] = range(1, len(input_file) + 1)
            input_file["peak"] = "peak_dREG_" + input_file['peak'].astype(str)
            
            for number in chromosomes :
                new_chr = number - 32
                input_file.loc[input_file['Chr'] == f"CM0206{number}.1", 'Chr'] = f"chromosome_{new_chr}"
        
        return input_file, df_name


    def save_df(self):
        """
        Save dataframe to tsv file
        """
        file_output, name = self.extract_infos(self.file_input)
        file_output.to_csv(f"{name}_extract.tsv", sep="\t", header=None, index=False)


    def delete_repeats(self):
        """
        Enter dataframe and delete duplicates rows based on dREG peaks name
        """
        file = pd.read_csv(self.file_input, sep="\t", header = None)
        file = file.drop_duplicates(subset=[3], keep='first')



if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="Add the file to reformat",
                                 formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    parser.add_argument("-f", "--filename", help="add tsv / bed file")
    args = parser.parse_args()

    Dataframe(args.filename).save_df()
    # Dataframe(args.filename).delete_repeats()
